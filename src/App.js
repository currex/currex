import logo from './logo.svg';
import './App.css';

function App() {
  return (
  <div className="content">
	    <h2>Zaloguj się do Currex</h2>
	    <p>Wprowadź login i hasło, aby się zalogować.</p> 
	  <form>
	    <div className="form-group">
		    <label for="exampleInputLogin1">Login</label>
		     <input type="text" className="form-control" id="exampleInputLogin1" aria-describedby="emailHelp" placeholder="Wprowadź login"/>
	    </div>
	    <div className="form-group">
		    <label for="exampleInputPassword1">Hasło</label>
		    <input type="password" className="form-control" id="exampleInputPassword1" placeholder="Wprowadź hasło"/>
	    </div>
	    <button type="submit" className="btn btn-danger">Zaloguj</button>
	  </form>

</div>
  );
}

export default App;
